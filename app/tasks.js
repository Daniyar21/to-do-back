const express = require('express');
const Task = require('../models/Task');
const User = require("../models/User");
const router = express.Router();

router.get('/', async (req, res) =>{

  const token = req.get('Authorization');

  if (!token) {
    return res.status(401).send({error: 'No token present'});
  }

  const user = await User.findOne({token});

  if (!user) {
    return res.status(401).send({error: 'Wrong token'});
  }

  try{
    const obj ={user: user._id};
    const tasks = await Task.find(obj);
    res.send(tasks);
  }catch (e){
    res.sendStatus(500);
  }

});

router.post('/', async (req,res)=>{

  const token = req.get('Authorization');

  if (!token) {
    return res.status(401).send({error: 'No token present'});
  }

  const user = await User.findOne({token});

  if (!user) {
    return res.status(401).send({error: 'Wrong token'});
  }

  try{
  const taskData = {
    user: user._id,
    title: req.body.title,
    description: req.body.description,
    status: req.body.status,
  };
  const task = new Task(taskData);
    await task.save();
    res.send(task);

  }catch (e) {
    res.status(400).send(e);
  }
});

router.put('/:id', async (req,res)=> {

  const token = req.get('Authorization');

  if (!token) {
    return res.status(401).send({error: 'No token present'});
  }

  const user = await User.findOne({token});

  if (!user) {
    return res.status(401).send({error: 'Wrong token'});
  }

  try{
    const obj={_id: req.params.id,user: user._id};
    delete req.body.user;
    const task = await Task.findOneAndUpdate(obj,req.body);

    if(!task){
      res.status(404).send({error: 'Task not found'});
    }
  }catch (e) {
    res.sendStatus(500);
  }

});

router.delete('/:id', async (req,res)=> {

  const token = req.get('Authorization');

  if (!token) {
    return res.status(401).send({error: 'No token present'});
  }

  const user = await User.findOne({token});

  if (!user) {
    return res.status(401).send({error: 'Wrong token'});
  }

  try{
    const obj={_id: req.params.id,user: user._id};
    const task = await Task.findOneAndDelete(obj);

    if(!task){
      res.status(404).send({error: 'Task not found'});
    }
  }catch (e) {
    res.sendStatus(500);
  }

})

module.exports = router;