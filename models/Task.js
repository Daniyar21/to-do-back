const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const TaskSchema = new mongoose.Schema({

  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
    enum: ['new', 'in_progress', 'complete']
  }
});

TaskSchema.plugin(idValidator);
const Task = mongoose.model('Product', TaskSchema);
module.exports = Task;