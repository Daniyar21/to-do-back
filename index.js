const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const exitHook = require('async-exit-hook');
const app = express();
const users = require('./app/users');
const tasks = require('./app/tasks');

app.use(express.json());
app.use(express.static('public'));
app.use(cors());
app.use('/users',users);
app.use('/tasks',tasks);



const port = 8000;

const run = async ()=>{
    await mongoose.connect('mongodb://localhost/taskApp')

    app.listen(port, ()=>{
        console.log('Server started on '+port+' port!');
    });

    exitHook(()=>{
        console.log('exiting');
        mongoose.disconnect();
    })
};

run().catch(e=> console.error(e));